// angular shims
require('../../node_modules/core-js/client/shim.min.js');
require('../../node_modules/zone.js/dist/zone.js');
require('../../node_modules/reflect-metadata/Reflect.js');

// angular
import { platformBrowserDynamic }    from '@angular/platform-browser-dynamic';
import {AppModule} from './components/app.module';
platformBrowserDynamic().bootstrapModule(AppModule);



