import { NgModule }      from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ColorPaletteSwatchesComponent } from  './colorpalette-swatches.component';
import { ColorPaletteControlsComponent } from  './colorpalette-controls.component';
import { ColorPaletteCSSComponent } from  './colorpalette-css.component';
import
	{ AppComponent } from './app.component';

@NgModule({
	imports: [ BrowserModule, FormsModule ],
	declarations: [ AppComponent, ColorPaletteSwatchesComponent, ColorPaletteControlsComponent, ColorPaletteCSSComponent ],
	bootstrap: [ AppComponent ]
})
export class AppModule { }
