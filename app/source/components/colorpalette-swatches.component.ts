import {Component, Input} from '@angular/core';

@Component({
    selector: 'colorpalette-swatches',
    template: `
      <div class="colorPalettePreview">
        <div class="colorPalettePreviewPrime">
			<div class="colorPalatteSwatches">
				<div *ngFor="let version of colorVersions" class="colorPaletteSwatch{{baseCssClass}}{{version.name}}" [style.background-color]="version.hex">{{version.name}}</div>
			</div>
        </div>
      </div>
    `
})

export class ColorPaletteSwatchesComponent {
	@Input()
	private colorVersions:Array<Object>;
	@Input()
	private baseCssClass:string;
}
