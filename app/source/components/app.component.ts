import {Component, OnInit} from '@angular/core';
import { ColorObject } from './colorobject';
import { ColorPaletteService } from './colorpalette.service';

@Component({
    selector: 'my-app',
    template: `
    <div class="row"><h2>{{title}}</h2></div>
<div class="row">
  <div id="colorPalette" >
    <div class="small-12 medium-6 large-4 columns"> 
      <colorpalette-swatches  [colorVersions]="colorVersions" [baseCssClass]="baseCssClass"></colorpalette-swatches>
    </div>
    <div class="small-12 medium-6 large-5 columns">
      <colorpalette-controls [controlColor]="colorObject" (generateColors)='generateColors()'></colorpalette-controls>
    </div>
    <div class="small-12 medium-12 large-3 columns">
      <colorpalette-css [cssClassArray]="cssClassArray"></colorpalette-css>
    </div>
  </div>
    `,
    providers: [ColorPaletteService]
})

export class AppComponent implements OnInit{
	private title:string = "Color Palette Generation";
	private colorObject:ColorObject = {h: 0, s: 100, v: 100 };
	private vModifiers: Array<Object> = [];
	private colorVersions:Array<Object>;
	private baseCssClass: string = 'primaryColor';
	private cssClassArray: Array<Object>;

	constructor(private colorPaletteService: ColorPaletteService) { }

	ngOnInit():void {
		this.generateColors();
	}
	generateColors() {
		this.colorVersions = [];
		this.cssClassArray = [];
		this.colorVersions = this.colorPaletteService.getVariation(this.colorObject);
		this.cssClassArray = this.colorPaletteService.createCss(this.baseCssClass,this.colorVersions);
	}
}
