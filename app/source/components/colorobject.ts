export class ColorObject {
	name?: string;
	h?: number;
	s?: number;
	v?: number;
	r?: string;
	g?: string;
	b?: string;
	hex?: string;
}