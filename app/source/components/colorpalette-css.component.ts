import {Component, Input, OnChanges} from '@angular/core';

@Component({
    selector: 'colorpalette-css',
    template: `
      <h6>Color CSS</h6>
      <div class="colorPaletteCSS">
      	<pre>
      		{{cssHTML}}
      	</pre>
      </div>
    `
})

export class ColorPaletteCSSComponent implements OnChanges {
	@Input()
	private cssClassArray:Array<Object>;
	private cssHTML:string = '';
	ngOnChanges() {
    this.cssHTML = '';
		for (let i of this.cssClassArray) {
			this.cssHTML += i;
		};
	}
}
