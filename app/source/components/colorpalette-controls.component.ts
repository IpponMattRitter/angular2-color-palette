import {Component, AfterContentInit, Input, Output,  EventEmitter } from '@angular/core';
import { ColorObject } from './colorobject';
const ColorUtil = require('../colorUtil.js'),
	colorUtil = new ColorUtil();

@Component({
    selector: 'colorpalette-controls',
    template: `
    <div class="colorPaletteControl">
        <div class="row">
			<div class="small-6 medium-6 large-4 columns">
				<div class="colorPaletteSwatch" [style.background-color]="hiddenColorValue"></div>
			</div>
			<div class="small-6 medium-6 large-4 columns">
				<button type="button" class="success button"  id="generatePaletteButton" (click)="generate()" >Generate</button>
			</div>
        </div>
        <div class="row">
			<div class="colorPaletteControlHSV" id="colorPaletteControlHSV">
				<label for="colorPaletteHValue" class="inline-form-element">Hue</label>
				<input name="colorPaletteHValue" #colorPaletteHValue id="colorPaletteHValue" type="text" size="4" class="inline-form-element">
				<div id="colorPaletteHSlider" class="slider" data-slider data-start='0' data-end='359' >
					<span class="slider-handle"  data-slider-handle role="slider" tabindex="1" aria-controls="colorPaletteHValue" (ondrag)='colorChange()'></span>
					<span class="slider-fill" data-slider-fill></span>
				</div>
				<label for="colorPaletteSValue" class="inline-form-element">Saturation</label>
				<input name="colorPaletteSValue" #colorPaletteSValue id="colorPaletteSValue" type="text" size="4" class="inline-form-element">
		 		<div id="colorPaletteSSlider" class="slider" data-slider data-start='0' data-end='100' data-initial-start='100'>
					<span class="slider-handle"  data-slider-handle role="slider" tabindex="1" aria-controls="colorPaletteSValue" ></span>
					<span class="slider-fill" data-slider-fill></span>
				</div>
				<label for="colorPaletteVValue" class="inline-form-element">Brightness</label>
				<input name="colorPaletteVValue" #colorPaletteVValue id="colorPaletteVValue" type="text" size="4" class="inline-form-element">
				<div id="colorPaletteVSlider" class="slider" data-slider data-start='0' data-end='100' data-initial-start='100'>
					<span class="slider-handle"  data-slider-handle role="slider" tabindex="1" aria-controls="colorPaletteVValue"></span>
					<span class="slider-fill" data-slider-fill></span>
				</div>
			</div>
        </div>
        <input type="hidden" id="colorPaletteHexValue" name="colorPaletteHexValue" [(ngModel)]="hiddenColorValue" />
	</div>
    `
})

export class ColorPaletteControlsComponent implements AfterContentInit {
	@Input()
	private controlColor: ColorObject;
	@Output()
	generateColors = new EventEmitter<boolean>();
	private hiddenColorValue: string = '';

	private processColor(color:ColorObject){
		color = colorUtil.hsvToRgb(color.h, color.s, color.v);
		this.controlColor.r = color.r;
		this.controlColor.g = color.g;
		this.controlColor.b = color.b;
		this.controlColor.hex = colorUtil.rgbToHex(this.controlColor.r, this.controlColor.g, this.controlColor.b);
		this.hiddenColorValue = this.controlColor.hex;
	}

	generate() {
		this.generateColors.emit();
	}
	ngAfterContentInit():void {
		(<any>$('#colorPaletteControlHSV')).foundation();
		$('#colorPaletteHSlider').on('moved.zf.slider', (event) => {
			this.controlColor.h = Number($(event.target).find('.slider-handle').attr('aria-valuenow'));
			this.processColor(this.controlColor);
		});
		$('#colorPaletteSSlider').on('moved.zf.slider', (event) => {
			this.controlColor.s = Number($(event.target).find('.slider-handle').attr('aria-valuenow'));
			this.processColor(this.controlColor);
		});
		$('#colorPaletteVSlider').on('moved.zf.slider', (event) => {
			this.controlColor.v = Number($(event.target).find('.slider-handle').attr('aria-valuenow'));
			this.processColor(this.controlColor);
		});
	}
}
