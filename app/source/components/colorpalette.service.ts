import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {ColorObject} from './colorobject';
const ColorUtil = require('../colorUtil.js'),
	colorUtil = new ColorUtil();

@Injectable()
export class ColorPaletteService {
	getModifiers() {
		let vModifiers: Array<Object> = [
		{
			name: 'v500',
			h: null,
			s: null,
			v: null,
		},
		{
			name: 'v400',
			h: null,
			s: '-20%',
			v: '7%'
		},
		{
			name: 'v300',
			h: null,
			s: '-40%',
			v: '13%'
		},
		{
			name: 'v200',
			h: null,
			s: '-59%',
			v: '23%'
		},
		{
			name: 'v100',
			h: null,
			s: '-77%',
			v: '31%'
		},
		{
			name: 'v50',
			h: null,
			s: '-99%',
			v: '40%'
		},
		{
			name: 'v600',
			h: '-2%',
			s: '1%',
			v: '-3%'
		},
		{
			name: 'v700',
			h: '-3%',
			s: '3%',
			v: '-8%'
		},
		{
			name: 'v800',
			h: '-5%',
			s: '5%',
			v: '-13%'
		},
		{
			name: 'v900',
			h: '-9%',
			s: '10%',
			v: '-21%'
		},
		{
			name: 'a1',
			h: null,
			s: '-38%',
			v: '43%'
		},
		{
			name: 'a2',
			h: null,
			s: '-4%',
			v: '42%'
		}
		];
		return vModifiers;
	}
	getVariation(oColor: ColorObject, modifiers?: Array<Object>) {
		if (!modifiers){
			modifiers = this.getModifiers();
		}
		let colorVersions:Array<Object> = [];
		for (let i of modifiers) {
			let oColorRGB: ColorObject,
				newColor: ColorObject = {},
				newHSVValues: Object = i;
			for (let prop in newHSVValues) {
				if (newHSVValues.hasOwnProperty(prop)) {
					if (prop === 'name') {
						newColor[prop] = newHSVValues[prop];
						continue;
					};
					if (typeof newHSVValues[prop] === 'string' && newHSVValues[prop].indexOf('%') >= 0) {
						newColor[prop] = oColor[prop] + (Math.round(oColor[prop] * (parseFloat(newHSVValues[prop]) / 100)));
					};
					if (typeof newHSVValues[prop] === 'number') {
						newColor[prop] = oColor[prop] + newHSVValues[prop];
					};
					if (!newHSVValues[prop]) {
						newColor[prop] = oColor[prop];
					};
				}
			}
			// if brightness is more than 100 (brighter than the sun yo) decrease saturation to make 
			// it appear lighter.
			if (newColor.v > 100) {
				newColor.s -= (newColor.v - 100) / 10;
				newColor.v = 100;
			}
			newColor.s = Math.max(newColor.s, 10);
			oColorRGB = colorUtil.hsvToRgb(newColor.h, newColor.s, newColor.v);
			newColor.hex = colorUtil.rgbToHex(oColorRGB.r, oColorRGB.g, oColorRGB.b);
			newColor.r = oColorRGB.r;
			newColor.g = oColorRGB.g;
			newColor.b = oColorRGB.b;
			colorVersions.push(newColor);
		}
		return colorVersions;
	}
	createCss(baseCSSClass: string, colorArray: Array<ColorObject>) {
		let cssClassArray:Array<Object> = [];
		for (let i of colorArray) {
			cssClassArray.push('.' + i.name + '{\n\tcolor: ' + i.hex + '\n}\n');
		}
		return cssClassArray;
	}
}
