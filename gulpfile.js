const gulp = require('gulp'),
	tslint = require('gulp-tslint'),
	webpack = require('gulp-webpack'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-sass'),
	del = require('del'),
	// variables for source and public directores
	sourceDir = "./app/source",
	publicDir = "./app/public";

// clean build
gulp.task('clean', function (){
	return del(publicDir + '/**/*.*')
});

// move static files
gulp.task('static', ['clean'], function () {
	gulp.src(sourceDir + '/**/*.html')
	.pipe(gulp.dest(publicDir))
});

// typescript linting
gulp.task('tslint', function (options) {
	return gulp.src(sourceDir + '/*.ts')
	.pipe(tslint(options))
	.pipe(tslint.report("verbose"))
});

// sass/scss compiling
gulp.task('sass', ['clean'], function () {
	return gulp.src(sourceDir + '/sass/**/*.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(gulp.dest(publicDir + '/css'))
});

// webpack
gulp.task('webpack', ['clean', 'tslint'], function () {
	return gulp.src(sourceDir + '/')
	.pipe(webpack({
		// entry is the file(s) that you will to have bundles made from.
		// main.ts is the main angular app while angularShim are necessary for angular to work in browser
		entry: {
			app: sourceDir + '/main.ts',
			//angularShim: sourceDir + '/angularShim.js'
		},
		output: {
			filename: '[name].js'
		},
		resolve: {
			extensions: ['','webpack.js', 'web.js', '.ts', '.js']
		},
		module: {
			loaders: [
				{ test: /\.ts$/, loader: 'ts-loader'}
			]
		}
	}))
	.pipe(uglify())
	.pipe(gulp.dest(publicDir + '/js/'))
});

// gulp watch
gulp.task('watch', function() {
	gulp.watch(sourceDir + '/sass/**/*.scss', ['sass']);
	gulp.watch(soruceDir + '**/*.ts', ['tslint']);
});

// gulp build
gulp.task('build', ['sass','webpack','static'],function(){

});
